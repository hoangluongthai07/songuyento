﻿Console.OutputEncoding = System.Text.Encoding.UTF8;
    Console.Write("Nhập vào số nguyên dương N: ");
    int N = int.Parse(Console.ReadLine());
    bool isPrime = true;
    for (int i = 2; i < N; i++) {
      if (N % i == 0) {
        isPrime = false;
        break;
      }
    }
    if (isPrime) {
      Console.WriteLine($"{N} là số nguyên tố");
    } else {
      Console.WriteLine($"{N} không phải là số nguyên tố");
    }